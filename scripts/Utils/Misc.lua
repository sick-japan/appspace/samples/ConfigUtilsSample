-- AppSpace API
--luacheck: ignore Container File Engine Object MLog

-- For ConfigUtils
--luacheck: globals isReal getValueType isArray table2Container auto2Container container2Auto
-- For File operation
--luacheck: globals checkFileAndMkdir checkFilePathAndMkdir fileCopy
-- For Image and PointCloud operation
--luacheck: globals createHeightMap cropBox

function isReal(_value)
  if type(_value)~="number" then
    return false
  else
    return _value%1 ..""~="0"
  end
end

function getValueType(_v)
  if type(_v) == "string" then
    return "STRING"
  elseif type(_v) == "number" then
    if isReal(_v) then
      return "FLOAT"
    else
      return "INT"
    end
  elseif type(_v) == "boolean" then
    return "BOOL"
  elseif type(_v) == "userdata" then
    return "OBJECT"
  end
end

function isArray(_table)
  local arrayType = nil
  for k,v in pairs(_table) do
    if k ~= v then
      return false, nil
    end
    if arrayType == nil then
      arrayType = type(v)
    elseif arrayType ~= type(v) then
      return false, nil
    end
  end
  return true, arrayType
end

function table2Container(_table)
  local container = Container.create()
  for k,v in pairs(_table) do
    if type(v) == "table" then
      local isarray, arraytype = isArray(v)
      if isarray then
        container:add(k, v, arraytype)
      else
        local ct = table2Container(v)
        container:add(k, ct, arraytype)
      end
    else
      container:add(k,v,getValueType(v))
    end
  end
  return container
end

function auto2Container(_value)
  -- check table or not
  if type(_value) == "table" then
    return table2Container(_value)
  else
    local container = Container.create()
    container:add(type(_value).."1",_value, getValueType(_value))
    return container
  end
end

function container2Auto(_container)
  local keyList = Container.list(_container)
  -- check keysize
  if #keyList == 1 then
    -- return single value (not table)
    return Container.get(_container, keyList[1])
  else
    -- prepare table to return multiple value
    local tb = {}
    for _,key in pairs(keyList) do
      local value = Container.get(_container, key)
      -- check object
      if type(value) == "userdata" then
        -- check container
        if Object.getType(value) == "Container" then
          -- Recursive call
          tb[key] = container2Auto(value)
        else
          -- handle just object
          tb[key] = value
        end
      else
        -- handle single value (not object)
        tb[key] = value
      end
    end
    -- return multiple value (table)
    return tb
  end
end

function checkFilePathAndMkdir(_filepath)
  if not File.exists(_filepath) then
    -- not exited
    if not File.mkdir(_filepath) then
      -- failed to make
      MLog.Error(Engine.getCurrentAppName(), "@Misc.lua/fileCopy()","Failed to make direcotry ".._filepath)
      return false
    end
    -- successfully made
    return true
  else
    -- existed
    return true
  end
end

function checkFileAndMkdir(_filename)
  local path, _, _ = File.extractPathElements(_filename)
  return checkFilePathAndMkdir(path)
end

function fileCopy(_Fromfilename, _Tofilename)
  if not File.exists(_Fromfilename) then
    -- Could not find _Fromfile
    MLog.Error(Engine.getCurrentAppName(), "@Misc.lua/fileCopy()","Could not find ".._Fromfilename)
    return false
  end
  -- check file and create direcotry if needed
  if not checkFileAndMkdir(_Tofilename) then
    -- Failed to make direcotry
    MLog.Error(Engine.getCurrentAppName(), "@Misc.lua/fileCopy()","Failed to make direcotry")
    return false
  end
  local success = File.copy(_Fromfilename, _Tofilename)
  if not success then
    -- Failed to copy
    MLog.Error(Engine.getCurrentAppName(), "@Misc.lua/fileCopy()","Failed to copy")
    return false
  end
  return true
end


function cropBox(_cloud, _box, _negative)
  local inliers = _cloud:cropShape(_box)
  local size,_,_ = _cloud:getSize()
  if size == 0 then
    MLog.Error(Engine.getCurrentAppName(), "@Misc.lua/cropBox()", "Could not crop. input cloud size == 0")
    return false
  end
  if #inliers > 0 then
    return true, _cloud:extractIndices(inliers, _negative)
  else
    MLog.Error(Engine.getCurrentAppName(), "@Misc.lua/cropBox()", "Could not crop. inliers == 0")
    return true, nil
  end
end

function createHeightMap(_cloud, _nonMissingData, _splatSize)
  if _cloud == nil then
    MLog.Error(Engine.getCurrentAppName(), "@Misc.lua/createHeightMap()", "Input cloud is nil")
    return false, nil
  end
  local size,_,_ = _cloud:getSize()
  if size == 0 then
    MLog.Error(Engine.getCurrentAppName(), "@Misc.lua/createHeightMap()", "Too few point size == " .. size)
    return false, nil
  end
  local bbox = _cloud:getBoundingBox()
  local pixelSizes = {1.0,1.0,1.0} -- x,y,z
  local splatSizes = {_splatSize}
  local mode = nil -- default 'TOPMOST'
  local generateIntensity = false
  -- luacheck: ignore success
  local success = nil
  local heightmap, _ = _cloud:toImage(bbox, pixelSizes, splatSizes, mode, generateIntensity)
  if heightmap == nil then
    success = false
    MLog.Error(Engine.getCurrentAppName(), "@Misc.lua/createHeightMap()", "Could not create height map")
  else
    success = true
  end
  if _nonMissingData then
    heightmap:missingDataReduce('MEDIAN',11)
    heightmap:missingDataSetAllInplace(0)
  end
  --heightmap = heightmap:mirror(false)
  return success, heightmap
end
