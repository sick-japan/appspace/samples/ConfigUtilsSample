--[[-------------------------------------------------------------------------------------------
-- Application Name: ConfigUtils
-- Author: Yuji Ogusu (SICK K.K.)
-- Version: V1.1
-- Date: 2021.07.18
--]]-------------------------------------------------------------------------------------------

-- luacheck: globals ConfigUtils

--require MLog
-- luacheck: read globals MLog

--require(Utils.Misc)
--luacheck: globals isReal getValueType isArray table2Container auto2Container container2Auto
--luacheck: globals checkFileAndMkdir checkFilePathAndMkdir fileCopy

-- AppSpace API
-- luacheck: read globals File Engine Object

ConfigUtils = {}

function ConfigUtils.copyDefault(self)
  local success = fileCopy(self.defaultFilename, self.configFilename)
  if success then
    MLog.Info(Engine.getCurrentAppName(),"@ConfigUtils.lua/copyDefault()", "Copy " .. self.defaultFilename)
  else
    MLog.Error(Engine.getCurrentAppName(),"@ConfigUtils.lua/copyDefault()", "Failed to copy " .. self.defaultFilename)
  end
  return success
end

function ConfigUtils.read(self, _version)
  local container = Object.load(self.configFilename, "JSON")
  if container == nil then
    MLog.Error(Engine.getCurrentAppName(),"@ConfigUtils.lua/read()", "Failed to load " .. self.configFilename)
    local success = self:copyDefault()
    if not success then
      MLog.Error(Engine.getCurrentAppName(),"@ConfigUtils.lua/read()", "Failed to copy " .. self.defaultFilename)
    end
    container = Object.load(self.configFilename, "JSON")
    if container == nil then
      MLog.Error(Engine.getCurrentAppName(),"@ConfigUtils.lua/read()", "Failed to load " .. self.defaultFilename)
      return false, nil
    end
  end
  local config = container2Auto(container)
  -- Validate version
  if config.version ~= _version then
    MLog.Error(Engine.getCurrentAppName(),"@ConfigUtils.lua/read()", "Failed to validate version. copy default.")
    local success = self:copyDefault()
    if not success then
      MLog.Error(Engine.getCurrentAppName(),"@ConfigUtils.lua/read()", "Failed to copy " .. self.defaultFilename)
    end
    container = Object.load(self.configFilename, "JSON")
    if container == nil then
      MLog.Error(Engine.getCurrentAppName(),"@ConfigUtils.lua/read()", "Failed to load " .. self.defaultFilename)
      return false, nil
    end
    config = container2Auto(container)
    if config.version ~= _version then
      MLog.Error(Engine.getCurrentAppName(),"@ConfigUtils.lua/read()", "WRONG VERSION!")
    end
  else
    MLog.Info(Engine.getCurrentAppName(),"@ConfigUtils.lua/read()",
      "Successfully validated configuration file version = " .. _version)
  end

  return true, config
end

function ConfigUtils.save(self, _config)
  local container = auto2Container(_config)
  local success = Object.save(container, self.configFilename)
  if not success then
    MLog.Error(Engine.getCurrentAppName(), "@ConfigUtils.lua/save()",
      "Failed to save " .. self.configFilename)
    success = self:copyDefault()
    if not success then
      MLog.Error(Engine.getCurrentAppName(),"@ConfigUtils.lua/save()", "Failed to copy " .. self.defaultFilename)
    end
    return false
  end
  MLog.Info(Engine.getCurrentAppName(), "@ConfigUtils.lua/save()",
    "Successfuly write to " .. self.configFilename)
  return true
end

function ConfigUtils.createConfigJSON(_config, _filename)
  checkFileAndMkdir(_filename)
  local configContainer = auto2Container(_config)
  local success = Object.save(configContainer, _filename, 'JSON')
  if success then
    MLog.Info(Engine.getCurrentAppName(),"@ConfigUtils.lua/createConfigJSON()", "Successfully created" .. _filename)
  else
    MLog.Error(Engine.getCurrentAppName(),"@ConfigUtils.lua/createConfigJSON()", "Failed to create " .. _filename)
  end
end

function ConfigUtils.create(_configFile, _defaultFile)
  local obj = {}
  setmetatable(obj, {__index = ConfigUtils})
  obj.configFilename = _configFile
  obj.defaultFilename = _defaultFile
  if not File.exists(obj.defaultFilename) then
    MLog.Error(Engine.getCurrentAppName(),"@ConfigUtils.lua/create()",
      "Could not find default config file .. "..obj.defaultFilename)
    return
  end

  -- check file and copy default configuretion file if needed
  if not File.exists(obj.configFilename) then
    MLog.Info(Engine.getCurrentAppName(),"@ConfigUtils.lua/create()", "Could not find " .. obj.configFilename)
    local success = obj:copyDefault()
    if not success then
      MLog.Error(Engine.getCurrentAppName(),"@ConfigUtils.lua/create()", "Failed to copy " .. obj.defaultFilename)
      return
    end
  end

  return obj
end

--EOF