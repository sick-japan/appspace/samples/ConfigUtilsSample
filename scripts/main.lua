-- AppSpace API using in this file
-- luacheck: ignore Script

-- Require Components : MLog
-- luacheck: ignore MLog

-- Require file : Utils/Misc.lua
require('Utils.Misc')
-- Require file : Utils/ConfigUtils.lua
-- luacheck: read globals ConfigUtils
require('Utils.ConfigUtils')

local configUtils = ConfigUtils.create("public/Config/sample.json", "resources/sample.default.json")

local appConfig

local function main()
  local CONFIG_VERSION = "1.0"
  local success
  success, appConfig = configUtils:read(CONFIG_VERSION)
  if not success then
    MLog.Error("ConfigUtilsSample","main.lua/main()", "Could not load configuration")
  end
  MLog.Debug("ConfigUtilsSample","main.lua/main()", "mode : " .. appConfig.mode
      .. " table1.a : " .. tostring(appConfig.table1.A)
      .. " table1.b : " .. tostring(appConfig.table1.B)
      .. " table1.c : " .. tostring(appConfig.table1.C)
    )
end
Script.register("Engine.OnStarted", main)
